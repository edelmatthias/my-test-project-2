/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models._
import org.apache.logging.log4j.scala.Logging

import scala.util.Try

object KafkaTopologyUtils extends Logging {

  import NtriplesConverter._

  def enrichWithMetadata(
      msgWithLocator: (String, List[ResourceWithLocator]),
      metadataFetcher: MetadataFetcher
  ): (String, List[Try[(String, List[Throwable])]]) =
    (
      msgWithLocator._1,
      LocatorExtraction
        .filterEnrichableResources(msgWithLocator._2)
        .map(rwl =>
          metadataFetcher
            .fetch(rwl.locator)
            .map(metadata =>
              convertToNtriples(
                metadata,
                rwl.recordId,
                rwl.resource,
                SettingsFromFile.getReportingStepName
              )
            )
        )
    )
}
