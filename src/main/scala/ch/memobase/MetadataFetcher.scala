/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.logging.log4j.scala.Logging

import scala.util.Try

class MetadataFetcher(
                       indexerHost: String,
                       readTimeoutMs: Int,
                       connectTimeoutMs: Int,
                       parserActionsLocal: List[String],
                       parserActionsRemote: List[String]
                     ) extends Logging {

  import MetadataFetcher._

  def fetch(url: String): Try[ujson.Value] = {
    Try {
      logger.info(s"Make request to indexer for $url")
      val startTime = System.currentTimeMillis()
      val res = requests.post(
        indexerHost,
        data = jsonBody(url)(
          if (!url.startsWith("http")) {
            parserActionsLocal
          }
          else {
            parserActionsRemote
          }
        ),
        readTimeout = readTimeoutMs,
        connectTimeout = connectTimeoutMs,
        keepAlive = true
      )
      logger.info(
        s"Got result from indexer with status code ${res.statusCode}. Took ${
          System
            .currentTimeMillis() - startTime
        }ms"
      )
      res match {
        case r if r.is2xx | r.is3xx =>
          logger.debug(r.text)
          ujson.read(r.text)
        case r =>
          throw ConnectionException(
            s"Connection error: Got ${r.statusCode} while connecting to ${r.url}"
          )
      }
    }
  }
}

object MetadataFetcher {
  def apply(
             indexerHost: String,
             readTimeoutMs: Int,
             connectTimeoutMs: Int,
             parserActionsLocal: List[String],
             parserActionsRemote: List[String]
           ): MetadataFetcher =
    new MetadataFetcher(
      indexerHost,
      readTimeoutMs,
      connectTimeoutMs,
      parserActionsLocal,
      parserActionsRemote
    )

  private val jsonBody: String => List[String] => ujson.Obj = url =>
    parserActions =>
      ujson.Obj(
        "url" -> url,
        "actions" -> parserActions,
        "downloadmime" -> "^image/.*$",
        "headersize" -> 10000
      )
}
