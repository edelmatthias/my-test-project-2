/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.streams.{KafkaStreams, StreamsConfig}
import org.apache.logging.log4j.scala.Logging

import java.time.Duration
import scala.util.{Failure, Success, Try}

object App extends scala.App with Logging {

  val topology = new KafkaTopology
  val metadataFetcher = MetadataFetcher(
    SettingsFromFile.getIndexerHost,
    SettingsFromFile.getIndexerReadTimeoutMs,
    SettingsFromFile.getIndexerConnectTimeoutMs,
    SettingsFromFile.getParserActionsLocal,
    SettingsFromFile.getParserActionsRemote
  )
  val streamsProperties = SettingsFromFile.getKafkaStreamsSettings
  streamsProperties.setProperty(
    StreamsConfig.consumerPrefix(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG),
    SettingsFromFile.getConsumerMaxPollIntervalMs
  )
  streamsProperties.setProperty(
    StreamsConfig.consumerPrefix(ConsumerConfig.MAX_POLL_RECORDS_CONFIG),
    SettingsFromFile.getConsumerMaxPollRecords
  )
  val streams = new KafkaStreams(
    topology.build(metadataFetcher),
    streamsProperties
  )
  val shutdownGracePeriodMs = 10000

  logger.trace("Starting stream processing")
  Try(
    streams.start()
  ) match {
    case Success(_) =>
      logger.info("Kafka Streams workflow successfully started.")
    case Failure(f) =>
      logger.error(s"Aborting due to errors: ${f.getMessage}")
      sys.exit(1)
  }

  sys.ShutdownHookThread {
    streams.close(Duration.ofMillis(shutdownGracePeriodMs))
  }
}
