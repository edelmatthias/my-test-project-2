/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models._
import ch.memobase.LocatorExtraction._
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala.kstream.KStream
import org.apache.kafka.streams.scala.{StreamsBuilder, _}
import org.apache.logging.log4j.scala.Logging

class KafkaTopology extends Logging {

  import KafkaTopologyUtils._
  import Serdes._
  logger.info(s"Current Step: ${SettingsFromFile.getReportingStepName}.")

  def build(metadataFetcher: MetadataFetcher): Topology = {
    val builder = new StreamsBuilder

    val source =
      builder.stream[String, String](SettingsFromFile.getKafkaInputTopic)

    val Array(enrichedRecord, partiallyEnrichedRecord, unenrichedRecord) =
      tryEnrichWithMetadata(source, metadataFetcher)

    sendEnrichedRecordsDownstream(enrichedRecord)
    sendEnrichedRecordsDownstream(partiallyEnrichedRecord)
    sendUnenrichedRecordsDownstream(unenrichedRecord)

    reportEnrichedRecords(enrichedRecord)
    reportRecordWithFailures(partiallyEnrichedRecord)
    reportRecordWithFailures(unenrichedRecord)

    builder.build()
  }

  private def tryEnrichWithMetadata(
      stream: KStream[String, String],
      metadataFetcher: MetadataFetcher
  ) =
    stream
      .mapValues(v => (v, System.currentTimeMillis()))
      .mapValues((k, v) => {
        val resourceWithLocators = getEbucoreLocators(k, v._1)
        val enrichedMetadata =
          enrichWithMetadata(resourceWithLocators, metadataFetcher)
        ((enrichedMetadata._1, ProcessingResult(enrichedMetadata._2)), v._2)
      })
      .branch(
        (_, v) => v._1._2.isInstanceOf[ProcessingSuccess],
        (_, v) => v._1._2.isInstanceOf[ProcessingPartialSuccess],
        (_, _) => true
      )

  private def sendRecordsDownstream(
      records: KStream[String, ((String, ProcessingResult), Long)]
  )(mappingFun: ((String, ProcessingResult)) => String): Unit =
    records
      .mapValues(v => mappingFun(v._1._1, v._1._2))
      .to(SettingsFromFile.getKafkaOutputTopic)

  private def sendEnrichedRecordsDownstream(
      enrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    sendRecordsDownstream(enrichedRecords)(x => s"${x._1}\n${x._2.getStatements}")

  private def sendUnenrichedRecordsDownstream(
      unenrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    sendRecordsDownstream(unenrichedRecords)(_._1)

  private def reportEnrichedRecords(
      enrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    enrichedRecords
      .mapValues((k, v) => {
        logger.info(
          s"Finished processing report $k in ${System.currentTimeMillis() - v._2}ms: SUCCESS (successfully enriched with metadata)"
        )
      })
      .map((k, _) =>
        (
          k,
          ReportingObject(
            k,
            ReportSuccess,
            "Record successfully enriched with metadata from media data."
          ).toString
        )
      )
      .to(SettingsFromFile.getKafkaReportTopic)

  private def reportRecordWithFailures(
      unenrichedRecords: KStream[String, ((String, ProcessingResult), Long)]
  ): Unit =
    unenrichedRecords
      .mapValues((k, v) => {
        logger.info(s"Finished processing report $k in ${System
          .currentTimeMillis() - v._2}ms: FAILURE (${v._1._2.getErrors})")
        v._1
      })
      .map((k, v) => {
        val exMsg = v._2.getErrors
        logger.warn(exMsg)
        logger.debug(v._2.errors.map(_.getStackTrace.mkString("\n")))
        (k, ReportingObject(k, ReportWarning, exMsg).toString)
      })
      .to(SettingsFromFile.getKafkaReportTopic)
}
