/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.settings.SettingsLoader

import java.util.Properties
import scala.collection.JavaConverters._

trait Settings {
  def getIndexerHost: String

  def getIndexerConnectTimeoutMs: Int

  def getIndexerReadTimeoutMs: Int

  def getKafkaStreamsSettings: Properties

  def getKafkaInputTopic: String

  def getKafkaOutputTopic: String

  def getKafkaReportTopic: String

  def getConsumerMaxPollIntervalMs: String

  def getConsumerMaxPollRecords: String

  def getParserActionsLocal: List[String]

  def getParserActionsRemote: List[String]

  def getReportingStepName: String
}

/**
 * Acts as a single point of fetching for the SettingsLoader
 */
object SettingsFromFile extends Settings {
  private val settings = new SettingsLoader(
    List(
      "consumerMaxPollIntervalMs",
      "consumerMaxPollRecords",
      "indexerHost",
      "indexerConnectTimeoutMs",
      "indexerReadTimeoutMs",
      "parserActionsLocal",
      "parserActionsRemote",
      "reportingStepName"
    ).asJava,
    "app.yml",
    false,
    true,
    false,
    false
  )

  def getApplicationId: String = sys.env.getOrElse("APPLICATION_ID", "")

  def getInaccessibleMediahosts: List[String] =
    settings.getAppSettings
      .getProperty("inaccessibleMediahosts")
      .split(",")
      .toList

  def getIndexerHost: String =
    settings.getAppSettings.getProperty("indexerHost")

  def getIndexerConnectTimeoutMs: Int =
    settings.getAppSettings.getProperty("indexerConnectTimeoutMs").toInt

  def getIndexerReadTimeoutMs: Int =
    settings.getAppSettings.getProperty("indexerReadTimeoutMs").toInt

  def getKafkaStreamsSettings: Properties = settings.getKafkaStreamsSettings

  def getKafkaInputTopic: String = settings.getInputTopic

  def getKafkaOutputTopic: String = settings.getOutputTopic

  def getKafkaReportTopic: String = settings.getProcessReportTopic

  def getReportingStepName: String = settings.getAppSettings.getProperty("reportingStepName")

  def getConsumerMaxPollIntervalMs: String =
    settings.getAppSettings.getProperty("consumerMaxPollIntervalMs")

  def getConsumerMaxPollRecords: String =
    settings.getAppSettings.getProperty("consumerMaxPollRecords")

  def getParserActionsLocal: List[String] =
    settings.getAppSettings.getProperty("parserActionsLocal").split(",").toList

  def getParserActionsRemote: List[String] =
    settings.getAppSettings.getProperty("parserActionsRemote").split(",").toList
}
