/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import cats.implicits._
import ch.memobase.models._
import org.joda.time.DateTime

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

object NtriplesConverter {

  /**
    * Converts and filters a JSON object to a Ntriples representation
    *
    * @param jsonObj the JSON object received from the Indexer
    * @return relevant Ntriples to send downstream
    */
  //scalastyle:off
  def convertToNtriples(
                         jsonObj: ujson.Value,
                         ricoRecordId: String,
                         ricoInstantiationId: String,
                         serviceId: String
                       ): (String, List[Throwable]) =
    jsonObj match {
      case o: ujson.Obj =>
        val (successes, failures) = o.value.iterator
          .collect {
            case ("errors", v: ujson.Obj) => handleErrors(v)
            case ("exif", v: ujson.Obj) =>
              handleExifData(v.value, ricoRecordId, ricoInstantiationId, serviceId)
            case ("ffprobe", v: ujson.Obj) =>
              handleFFprobeData(v.value, ricoRecordId, ricoInstantiationId)
            case ("height", v: ujson.Num) =>
              handleHeightData(v.value, ricoInstantiationId)
            case ("histogram", v: ujson.Arr) =>
              handleHistogramData(v.value, ricoRecordId, ricoInstantiationId)
            case ("identify", v: ujson.Obj) =>
              handleIdentifyData(v.value, ricoRecordId, ricoInstantiationId)
            case ("mimetype", v: ujson.Str) =>
              handleMimetypeData(v.value, ricoInstantiationId)
            case ("siegfried", v: ujson.Arr) =>
              handleSiegfriedData(v.value, ricoRecordId, ricoInstantiationId)
            case ("validateav", v: ujson.Obj)    => handleValidation(v.value)
            case ("validateimage", v: ujson.Obj) => handleValidation(v.value)
            case ("width", v: ujson.Num) =>
              handleWidthData(v.value, ricoInstantiationId)
            case (p, v) if relevantProperties contains p =>
              Failure(
                EnrichedMetadataException(
                  s"JSON type ${v.toString} not expected for property `$p` in $ricoRecordId"
                )
              )
          }
          .toList
          .partition(_.isSuccess)
        val ntripleStatements = successes.map(s => s.get).mkString.trim
        val errors = failures.map(f => f.failed.get)
        (ntripleStatements, errors)
      case _ => ("", List(EnrichedMetadataException("No valid JSON object")))
    }
  //scalastyle:on

  private def handleErrors(v: ujson.Obj): Try[String] = {
    if (v.value.nonEmpty) {
      Failure(ExtractionException(ujson.write(v)))
    } else {
      Success("")
    }
  }

  private def handleExifData(
      v: mutable.LinkedHashMap[String, ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String,
      serviceId: String
  ): Try[String] = {
    v.iterator
      .filter(t => exifProperties contains t._1)
      .map {
        case ("Artist", v: ujson.Str) =>
          createExifArtistNtriples(ricoRecord, v.value, serviceId)
        case ("Copyright", v: ujson.Str) =>
          createExifCopyrightNtriples(ricoInstantiation, v.value, serviceId)
        case ("CreatorCity", v: ujson.Str) =>
          createXmpCreatorPlaceNtriples(ricoRecord, v.value, serviceId)
        case ("CreatorCountry", v: ujson.Str) =>
          createXmpCreatorPlaceNtriples(ricoRecord, v.value, serviceId)
        case ("DateTimeOriginal", v: ujson.Str) =>
          createExifDateTimeOriginalNtriples(ricoRecord, v.value, serviceId)
        case (k, v) =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${v.toString} not expected for property `exif.$k` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap(l => Success(l.mkString))
  }

  private def handleFFprobeData(
      v: mutable.LinkedHashMap[String, ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.iterator
      .filter(_._1 == "format")
      .map {
        case (_, v: ujson.Obj) =>
          Success(
            v.value.iterator
              .collect {
                case ("duration", v: ujson.Str) =>
                  NtriplesStatement(
                    ricoInstantiation,
                    ebucore("duration"),
                    Literal(v.value)
                  ).toString
              }
              .take(1)
              .mkString
          )
        case (_, v) =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${v.toString} not expected for property `ffprobe.streams` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap(l => Success(l.head))
  }

  private def handleHeightData(
      v: Double,
      ricoInstantiation: String
  ): Try[String] =
    Success(
      NtriplesStatement(
        ricoInstantiation,
        ebucore("height"),
        Literal(v.toString)
      ).toString
    )

  private def handleHistogramData(
      v: mutable.ArrayBuffer[ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.iterator
      .map {
        case c: ujson.Obj if c.value.contains("code") =>
          Success(c.value("code"))
        case _: ujson.Obj =>
          Failure(
            EnrichedMetadataException(
              s"Property `histogram.<ind>.code` missing in $ricoRecord"
            )
          )
        case c =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${c.toString} not expected for property `histogram.<ind>` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap { l =>
        Success(
          l.map(c =>
            NtriplesStatement(
              ricoInstantiation,
              edm("componentColor"),
              Literal(
                c.value.toString.substring(1),
                "http://www.w3.org/2001/XMLSchema#hexBinary",
                DatatypeIndicator
              )
            ).toString
          ).mkString
        )
      }
  }

  private def handleIdentifyData(
      v: mutable.LinkedHashMap[String, ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.iterator
      .filter(_._1 == "image")
      .map {
        case (_, v: ujson.Obj) =>
          Success(v.value.iterator.collect {
            case ("orientation", v: ujson.Str) =>
              NtriplesStatement(
                ricoInstantiation,
                ebucore("orientation"),
                Literal(v.value)
              ).toString
            case ("type", v: ujson.Str) =>
              NtriplesStatement(
                ricoInstantiation,
                rdau("P60558"),
                Literal(v.value)
              ).toString
          }.mkString)
        case (_, v) =>
          Failure(
            EnrichedMetadataException(
              s"JSON type ${v.toString} not expected for property `identify.image` in $ricoRecord"
            )
          )
      }
      .toList
      .sequence
      .flatMap(l =>
        if (l.nonEmpty) {
          Success(l.head)
        } else {
          Failure(
            EnrichedMetadataException(
              s"Property `identify.image` contains no inner objects"
            )
          )
        }
      )
  }

  private def handleMimetypeData(
      v: String,
      ricoInstantiation: String
  ): Try[String] = {
    Success(
      NtriplesStatement(
        ricoInstantiation,
        ebucore("hasMimeType"),
        Literal(v)
      ).toString
    )
  }

  private def handleSiegfriedData(
      v: mutable.ArrayBuffer[ujson.Value],
      ricoRecord: String,
      ricoInstantiation: String
  ): Try[String] = {
    v.map {
      case e: ujson.Obj =>
        if (e.value.contains("id")) {
          e.value("id") match {
            case id: ujson.Str => Success(id.value)
            case _ =>
              Failure(
                EnrichedMetadataException(
                  s"Value of property `siegfried.<ind>.id` is not of type String"
                )
              )
          }
        } else {
          Failure(
            EnrichedMetadataException(
              s"No key `id` found in property siegfried in $ricoRecord"
            )
          )
        }
      case e =>
        Failure(
          EnrichedMetadataException(
            s"JSON type ${e.toString} not expected in property siegfried in $ricoRecord"
          )
        )
    }.toList
      .sequence
      .flatMap(l =>
        if (l.nonEmpty) {
          val id = l.get(0).get
          Success(
            NtriplesStatement(
              ricoInstantiation,
              ebucore("hasFormat"),
              Literal(id)
            ).toString
          )
        } else {
          Failure(
            EnrichedMetadataException(
              s"Property `siegfried` contains no inner objects"
            )
          )
        }
      )
  }

  private def handleValidation(
      v: mutable.LinkedHashMap[String, ujson.Value]
  ): Try[String] =
    v match {
      case o if o.contains("status") && o("status").value == "ok" => Success("")
      case o if o.contains("status") && o.contains("message") =>
        Failure(
          ValidationException(
            s"Validation exception: ${o("message").value.toString}"
          )
        )
      case o if o.contains("status") =>
        Failure(ValidationException("Validation exception with unknown cause"))
      case _ => Success("")
    }

  private def handleWidthData(
      v: Double,
      ricoInstantiation: String
  ): Try[String] =
    Success(
      NtriplesStatement(
        ricoInstantiation,
        ebucore("width"),
        Literal(v.toString)
      ).toString
    )

  private val relevantProperties = List(
    "errors",
    "exif",
    "ffprobe",
    "height",
    "histogram",
    "identify",
    "mimetype",
    "siegfried",
    "validateav",
    "validateimage",
    "width"
  )

  private val exifProperties = List(
    "Artist",
    "Copyright",
    "CreatorCity",
    "CreatorCountry",
    "DateTimeOriginal"
  )

  val dct: String => String = (p: String) => s"http://purl.org/dc/terms/1.0/$p"
  val ebucore: String => String = (p: String) =>
    s"http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#$p"
  val edm: String => String = (p: String) =>
    s"http://www.europeana.eu/schemas/edm/$p"
  val rdau: String => String = (p: String) =>
    s"http://rdaregistry.info/Elements/u/$p"
  val rdf: String => String = (p: String) =>
    s"http://www.w3.org/1999/02/22-rdf-syntax-ns#$p"
  val rico: String => String = (p: String) =>
    s"https://www.ica.org/standards/RiC/ontology#$p"

  private val createRicoActivity = (appId: String) => {
    val rdfTypeMechanism = BlankNode(rdf("type"), URI(rico("Mechanism")))
    val ricoName = BlankNode(rico("name"), Literal(appId))
    val rdfTypeActivity = BlankNode(rdf("type"), URI(rico("Activity")))
    val ricoPerformedBy =
      BlankNode(rico("performedBy"), rdfTypeMechanism, ricoName)
    val rdfTypeSingleDate = BlankNode(rdf("type"), URI(rico("SingleDate")))
    val ricoNormalizedDateValue =
      BlankNode(rico("normalizedDateValue"), Literal(new DateTime().toString))
    val ricoHasBeginningDate = BlankNode(
      rico("hasBeginningDate"),
      rdfTypeSingleDate,
      ricoNormalizedDateValue
    )
    BlankNode(
      rico("resultsFrom"),
      rdfTypeActivity,
      ricoPerformedBy,
      ricoHasBeginningDate
    )
  }

  private val createExifArtistNtriples =
    (subject: String, name: String, appId: String) => {
      val rdfTypeCreationRelation =
        BlankNode(rdf("type"), URI(rico("CreationRelation")))
      val ricoTypeExifArtist = BlankNode(rico("type"), Literal("exifArtist"))
      val ricoName = BlankNode(rico("name"), Literal(name))
      val ricoTypeAgent = BlankNode(rdf("type"), URI(rico("Agent")))
      val ricoCreationRelationHasTarget =
        BlankNode(rico("creationRelationHasTarget"), ricoTypeAgent, ricoName)
      Success(
        NtriplesStatement(
          subject,
          rico("recordResourceOrInstantiationIsSourceOfCreationRelation"),
          rdfTypeCreationRelation,
          ricoTypeExifArtist,
          ricoCreationRelationHasTarget,
          createRicoActivity(appId)
        ).toString
      )
    }

  private val createExifCopyrightNtriples =
    (subject: String, name: String, appId: String) => {
      val rdfTypeRule = BlankNode(rdf("type"), URI(rico("Rule")))
      val ricoTypeExifCopyright =
        BlankNode(rico("type"), Literal("exifCopyright"))
      val ricoName = BlankNode(rico("name"), Literal(name))
      Success(
        NtriplesStatement(
          subject,
          rico("regulatedBy"),
          rdfTypeRule,
          ricoTypeExifCopyright,
          ricoName,
          createRicoActivity(appId)
        ).toString
      )
    }

  private val createExifDateTimeOriginalNtriples =
    (subject: String, dateTime: String, appId: String) =>
      Try {
        val rdfTypeSingleDate = BlankNode(rdf("type"), URI(rico("SingleDate")))
        val dt = new DateTime(dateTime)
        val ricoNormalizedDateValue =
          BlankNode(rico("normalizedDateValue"), Literal(dt.toString))
        NtriplesStatement(
          subject,
          dct("created"),
          rdfTypeSingleDate,
          ricoNormalizedDateValue,
          createRicoActivity(appId)
        ).toString
      }

  private val createXmpCreatorPlaceNtriples =
    (subject: String, place: String, appId: String) => {
      val rdfTypePlace = BlankNode(rdf("type"), URI(rico("Place")))
      val ricoName = BlankNode(rico("name"), Literal(place))
      Success(
        NtriplesStatement(
          subject,
          rdau("P6055"),
          rdfTypePlace,
          ricoName,
          createRicoActivity(appId)
        ).toString
      )
    }

}
