/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import java.util.Properties

import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.apache.kafka.streams.scala.ImplicitConversions._
import org.apache.kafka.streams.scala._
import org.apache.kafka.streams.test._
import org.apache.kafka.streams.{StreamsConfig, Topology, TopologyTestDriver}
import org.scalatest.funsuite.AnyFunSuite

class KafkaTopologyTest extends AnyFunSuite {
  import org.apache.kafka.streams.scala.serialization.Serdes._

  private val fixture = new {
    private val props: Properties = {
      val p = new Properties()
      p.put(StreamsConfig.APPLICATION_ID_CONFIG, "test")
      p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234")
      p
    }
    val factory = new ConsumerRecordFactory[String, String](new StringSerializer, new StringSerializer)
    val testDriver: Topology => TopologyTestDriver = t => new TopologyTestDriver(t, props)
    val inputTopic = "input"
    val outputTopic = "output"

    def getOutput(ttd: TopologyTestDriver): ProducerRecord[String, String] = {
      ttd.readOutput(outputTopic, new StringDeserializer, new StringDeserializer)
    }
  }

  test("enrich record with analyzable media object successfully") {

    val builder: StreamsBuilder = new StreamsBuilder
    builder.stream[String, String](fixture.inputTopic)
      .filter((_, v) => v.nonEmpty)
      .to(fixture.outputTopic)

    val tD = fixture.testDriver(builder.build())
    tD.pipeInput(fixture.factory.create(fixture.inputTopic, "key", "value"))

    OutputVerifier.compareKeyValue(fixture.getOutput(tD), "key", "value")
    tD.close()
  }

}
