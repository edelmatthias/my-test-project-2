/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package ch.memobase

import ch.memobase.models._
import org.scalatest.funsuite.AnyFunSuite

class NtriplesObjectTest extends AnyFunSuite {

  private def fixture =
    new {
      val subject = "http://www.w3.org/2001/sw/RDFCore/ntriples/"
      val predicate = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
      val objUri = "http://xmlns.com/foaf/0.1/Document"
      val objLiteral = "astring"
      val objLiteralWithLinebreak: String =
        """astring
          |anotherString""".stripMargin
      val objLiteralNormalized = """astring\nanotherString"""
      val indicator = "en-US"
    }

  test(
    "A single N-Triples statement with URI as object should have valid syntax"
  ) {
    val obj = URI(fixture.objUri)
    val st = NtriplesStatement(fixture.subject, fixture.predicate, obj).toString
    assert(
      st == s"<${fixture.subject}> <${fixture.predicate}> <${fixture.objUri}> .\n"
    )
  }

  test(
    "A single N-Triples statement with literal as object should have valid syntax"
  ) {
    val obj = Literal(fixture.objLiteral)
    val st = NtriplesStatement(fixture.subject, fixture.predicate, obj).toString
    assert(
      st == s"""<${fixture.subject}> <${fixture.predicate}> "${fixture.objLiteral}" .\n"""
    )
  }

  test(
    "A single N-Triples statement with literal containing a linebreak as object should be normalized"
  ) {
    val obj = Literal(fixture.objLiteralWithLinebreak)
    val st = NtriplesStatement(fixture.subject, fixture.predicate, obj).toString
    assert(
      st == s"""<${fixture.subject}> <${fixture.predicate}> "${fixture.objLiteralNormalized}" .\n"""
    )
  }

  test(
    "A single N-Triples statement with annotated literal as object should have valid syntax"
  ) {
    val obj = Literal(fixture.objLiteral, fixture.indicator)
    val st = NtriplesStatement(fixture.subject, fixture.predicate, obj).toString
    assert(
      st == s"""<${fixture.subject}> <${fixture.predicate}> "${fixture.objLiteral}"@${fixture.indicator} .\n"""
    )
  }

  test("N-Triples statements with a blank node should have valid syntax") {
    val objUri = URI(fixture.objUri)
    val objLiteral = Literal(fixture.objLiteral, fixture.indicator)
    val bn1 = BlankNode(fixture.predicate, objUri)
    val bn2 = BlankNode(fixture.predicate, objLiteral)
    val bns = BlankNodes(List(bn1, bn2))
    val st = NtriplesStatement(fixture.subject, fixture.predicate, bns)
    val hashCode = "xyz"
    val expectedRes =
      s"""_:$hashCode <${fixture.predicate}> <${fixture.objUri}> .\n
         |_:$hashCode <${fixture.predicate}> "${fixture.objLiteral}"@${fixture.indicator} .\n
         |<${fixture.subject}> <${fixture.predicate}> _:$hashCode .\n""".stripMargin
    assert(
      st.obj
        .asInstanceOf[BlankNodes]
        .values
        .head
        .obj
        .asInstanceOf[URI]
        .value == fixture.objUri
    )
    assert(
      st.obj
        .asInstanceOf[BlankNodes]
        .values(1)
        .obj
        .asInstanceOf[Literal]
        .value == fixture.objLiteral
    )
  }
}
