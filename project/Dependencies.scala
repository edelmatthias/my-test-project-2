/*
 * media-metadata-extractor
 * Copyright (C) 2020  Memoriav
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import sbt._

object Dependencies {
  lazy val kafkaV = "2.7.0"
  lazy val log4jV = "2.11.2"
  lazy val scalatestV = "3.1.2"

  lazy val cats = "org.typelevel" %% "cats-core" % "2.1.1"
  lazy val jodaTime = "joda-time" % "joda-time" % "2.10.6"
  lazy val kafkaStreams = "org.apache.kafka" %% "kafka-streams-scala" % kafkaV
  lazy val kafkaStreamsTestUtils = "org.apache.kafka" % "kafka-streams-test-utils" % kafkaV
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jV
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jV
  lazy val log4jScala = "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
  lazy val log4jSlf4j = "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4jV
  lazy val mediaMetadataUtils = "ch.memobase" %% "media-metadata-utils" % "0.1.5"
  lazy val memobaseServiceUtils = "org.memobase" % "memobase-service-utilities" % "3.0.1"
  lazy val rdfRioApi = "org.eclipse.rdf4j" % "rdf4j-rio-api" % "3.2.3"
  lazy val rdfRioNtriples = "org.eclipse.rdf4j" % "rdf4j-rio-ntriples" % "3.2.3"
  lazy val requests = "com.lihaoyi" %% "requests" % "0.5.1"
  lazy val scalatic = "org.scalactic" %% "scalactic" % scalatestV
  lazy val scalaTest = "org.scalatest" %% "scalatest" % scalatestV
  lazy val scalaUri = "io.lemonlabs" %% "scala-uri" % "2.2.3"
  lazy val uPickle = "com.lihaoyi" %% "upickle" % "0.9.5"
}
