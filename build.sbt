import Dependencies._

ThisBuild / scalaVersion := "2.12.11"
ThisBuild / organization := "ch.memobase"
ThisBuild / organizationName := "Memoriav"
ThisBuild / git.gitTagToVersionNumber := { tag: String =>
  if (tag matches "[0-9]+\\..*") {
    Some(tag)
  } else {
    None
  }
}

lazy val root = (project in file("."))
  .enablePlugins(GitVersioning)
  .settings(
    name := "Media Metadata Extractor",
    scalacOptions += "-Ypartial-unification",
    assemblyJarName in assembly := "app.jar",
    test in assembly := {},
    assemblyMergeStrategy in assembly := {
      case "log4j.properties"  => MergeStrategy.first
      case "log4j2.xml"        => MergeStrategy.first
      case other if other.contains("module-info.class") => MergeStrategy.discard
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    },
    mainClass in assembly := Some("ch.memobase.App"),
    resolvers ++= Seq(
      "Memobase Libraries" at "https://gitlab.switch.ch/api/v4/projects/1324/packages/maven"
    ),
    libraryDependencies ++= Seq(
      cats,
      jodaTime,
      kafkaStreams,
      log4jApi,
      log4jCore,
      log4jSlf4j,
      log4jScala,
      mediaMetadataUtils,
      memobaseServiceUtils excludeAll (ExclusionRule(organization =
        "org.slf4j"
      )),
      requests,
      scalatic,
      scalaUri,
      uPickle,
      kafkaStreamsTestUtils % Test,
      rdfRioApi % Test,
      rdfRioNtriples % Test,
      scalaTest % Test
    )
  )
